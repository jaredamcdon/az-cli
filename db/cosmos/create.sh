#!/bin/bash

account=$(az account list | jq '.[].user.name')

json=$(az group list)

choices=$(echo $json | jq '.[].name' | tr -d '"')

IFS=$'\n' choices=($choices)

echo "Resource Groups:"

i=0
for choice in ${choices[@]}
do
	echo "$i: $choice"
	i=$((i + 1))
done

read -p "Resource group: " user_choice

resourceGroup=${choices[$user_choice]}

#rawTypes=$(az cosmosdb --help | grep resources)
#typeList=()
#i=0
#while IFS=$"\n" read -r line
#do
#	if [[ $line = *"Manage"* ]]
#	then
#		echo "$i: $line"
#		i=$((i + 1))
#		type=$(echo $line | head -n1 | awk '{print $1;}')
#		typeList+=("$type")
#	fi
#done <<< $rawTypes


#read -p "Type: " userType

#dbType=${typeList[userType]}

read -p "Name: " name

name="$name$RANDOM"

echo -e "Name:\t$name"
echo -e "Group:\t$resourceGroup"
#echo -e "Type:\t$dbType"
read -p "Continue? [y/n]" cont

if test "$cont" = "y"
then
	echo "Creating CosmosDB $name in $resourceGroup ..."

	az cosmosdb create \
		--name $name \
		--resource-group $resourceGroup \
		--enable-free-tier true \
		--default-consistency-level "Session" \

	#az cosmosdb $dbType create \
	#	--name $name \
	#	--resource-group $resourceGroup \
	#	--enable-free-tier true \
	#	--default-consistency-level "Session" \
	#	--account $account
fi
