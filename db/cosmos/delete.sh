#!/bin/bash

json=$(az cosmosdb list)
id=$(echo "$json" | jq '.[].writeLocations[].id' | tr -d '"')
name=$(echo "$json" | jq '.[].name')
group=$(echo "$json" | jq '.[].resourceGroup' | tr -d '"')

echo -e "ID:\t$id"
echo -e "Name:\t$name"
echo -e "Group:\t$group"

read -p "Delete? (y/n): " user_del

if test "$user_del" = "y"
then
	az cosmosdb delete \
		--name $name \
		--resource-group $group
fi
