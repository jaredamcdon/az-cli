json=$(az vm list)

name=$(echo $json | jq .[].name | tr -d '"')

group=$(echo $json | jq .[].resourceGroup | tr -d '"')

user=$(echo $json | jq .[].osProfile.adminUsername | tr -d '"')

az vm open-port \
	--port 80
	--resource-group $group \
	--name $name \
