json=$(az vm list)

name=$(echo $json | jq .[].name | tr -d '"')

group=$(echo $json | jq .[].resourceGroup | tr -d '"')

user=$(echo $json | jq .[].osProfile.adminUsername | tr -d '"')

echo "stopping vm ..."

az vm stop \
	--resource-group $group \
	--name $name

echo "querying vm ..."

az vm get-instance-view \
	--name $name \
	--resource-group $group \
	--query "instanceView.statuses[?starts_with(code, 'PowerState/')].displayStatus" -o tsv

echo "starting vm ..."

az vm start \
	--name $name \
	--group $group

echo "restarting vm ..."

az vm restart \
	--name $name \
	--resource-group $group
