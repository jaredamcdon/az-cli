# this needs modification for working with multiple vm's

json=$(az vm list)

name=$(echo $json | jq .[].name | tr -d '"')
echo "name: $name"

group=$(echo $json | jq .[].resourceGroup | tr -d '"')
echo "resourceGroup: $group"

user=$(echo $json | jq .[].osProfile.adminUsername | tr -d '"')
echo "user: $user"


ip=$(az vm list-ip-addresses -g $group -n $name | jq .[].virtualMachine.network.publicIpAddresses[0].ipAddress | tr -d '"' )
echo "ip: $ip"

echo -e "run ssh:\nssh $user@$ip"
