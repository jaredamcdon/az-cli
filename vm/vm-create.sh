json=$(az group list)

choices=$(echo $json | jq '.[].name' | tr -d '"')

IFS=$'\n'
choices=($choices)

echo "Resource Groups:"

i=0
for choice in ${choices[@]}
do
	        echo "$i: $choice"
		        i=$((i + 1))
		done

		read -p "Resource group: " user_choice

		resourceGroup=${choices[$user_choice]}

az vm create \
	--resource-group $resourceGroup \
	--location westus \
	--name SampleVM \
	--image UbuntuLTS \
	--admin-username azureuser \
	--generate-ssh-keys \
	--verbose
	# you can add additional paramters
	#--size "Standard_DS2_v2"
