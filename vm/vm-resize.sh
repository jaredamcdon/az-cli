json=$(az vm list)

name=$(echo $json | jq .[].name | tr -d '"')

group=$(echo $json | jq .[].resourceGroup | tr -d '"')

user=$(echo $json | jq .[].osProfile.adminUsername | tr -d '"')

az vm resize \
	--resource-group $group \
	--name $name \
	--size Standard_d2s_v3
