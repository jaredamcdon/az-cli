#!/bin/bash

echo -e "test repo if needed: https://github.com/Azure-Samples/php-docs-hello-world\n#"

json=$(az group list)

choices=$(echo $json | jq '.[].name' | tr -d '"')

IFS=$'\n' choices=($choices)

echo "Resource Groups:"

i=0
for choice in ${choices[@]}
do
	echo "$i: $choice"
	i=$((i + 1))
done

read -p "Resource group: " user_choice

resourceGroup=${choices[$user_choice]}

read -p "App name: " app_name
app_name="$app_name-$RANDOM"

read -p "Git repo: " git_repo

echo "Confirm selection: "
echo -e "group:\t$resourceGroup"
echo -e "name:\t$app_name"
echo -e "Repo:\t$git_repo"
read -p "Continue [y/n]: " cont

if test "$cont" = "y"
then
	az appservice plan create \
		--name $app_name \
		--resource-group $resourceGroup \
		--sku FREE
	az webapp create \
		--name $app_name \
		--resource-group $resourceGroup \
		--plan $app_name
	az webapp deployment source config \
		--name $app_name \
		--resource-group $resourceGroup \
		--repo-url $git_repo \
		--branch master \
		--manual-integration
	echo "http://$app_name.azurewebsites.net"

	read -p "Press enter to delete application."

	az webapp delete \
		--name $app_name \
		--resource-group $resourceGroup
fi
